module.exports = function(grunt) {

  grunt.initConfig({
    strip_code: {
      options: {
        blocks: [
          {
            start_block: "<title>",
            end_block: "<defs>"
          },
          {
            start_block: "</defs>",
            end_block: "</g>"
          }
        ]
      },
      your_target: {
        src: 'svgs/*.svg'
      },
    },
    svgstore: {
      options: {
        prefix : 'icon-',
        svg: {
          viewBox : '0 0 30 30',
          xmlns: 'http://www.w3.org/2000/svg'
        },
        formatting : {
          indent_size : 2
        },
        cleanup: true,
        includedemo: true
      },
      default: {
        files: {
          'dest/icons.svg': ['svgs/*.svg'],
        },
      },
    }
  });

  grunt.loadNpmTasks('grunt-strip-code');
  grunt.loadNpmTasks('grunt-svgstore');
};